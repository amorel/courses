# Sources des cours sur Vue.js, Progressive Web Apps et android

Le contenu des cours proposés sur l'ensemble du repository sont mis à disposition selon la [https://creativecommons.org/licenses/by-sa/4.0/](Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International).

Vous êtes libres de les modifier et partager comme bon vous semble.

## Choisir votre cours

Choisissez le cours que vous souhaitez présenter (chaque branche correspond à un cours)

## Builder le cours en local

Vous pouvez spécifier un numéro de port si 8080 ne vous convient pas

```console
npm install
npm start -- --port=8001
```

## Exporter en PDF

L'export en PDF peut être réalisé via decktap [[https://github.com/astefanutti/decktape]]

En local:

```console
decktape -s 1920x1080 reveal 'http://localhost:8000'  slides.pdf
```

Via docker:

```console
docker run --rm -t --net=host -v `pwd`:/slides astefanutti/decktape -s 1920x1080 http://localhost:8000 slides.pdf
```

## Contribuer

Des questions sur le cours, des corrections, des suggestions ? N'hésitez pas à [soumettre des tickets sur gitlab](https://gitlab.nuiton.org/amorel/courses/-/issues/new) !
